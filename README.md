# SOLOv2
The code is an unofficial pytorch implementation of [SOLOv2: Dynamic, Faster and Stronger](https://arxiv.org/abs/2003.10152)


## Install
Please check [SOLOv1](https://github.com/WXinlong/SOLO/blob/master/docs/INSTALL.md) for installation instructions.

## Training

```
python tools/train.py configs/solov2/solov2_r101_3x.py
```
